package com.example.voicecallapp

import android.Manifest
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothProfile
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.media.AudioDeviceInfo
import android.media.AudioManager
import android.os.Build
import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.voicecallapp.ui.theme.VoiceCallAppTheme
import io.agora.rtc2.ChannelMediaOptions
import io.agora.rtc2.Constants
import io.agora.rtc2.IRtcEngineEventHandler
import io.agora.rtc2.RtcEngine
import io.agora.rtc2.RtcEngineConfig
import okhttp3.OkHttpClient
import okhttp3.Request
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException

class MainActivity : ComponentActivity() {
    private var agoraEngine: RtcEngine? = null
    private val appId: String = "8707244643a740018b2a8c3a2ac972d9"
    private var serverUrl = "https://us-central1-rad-proto.cloudfunctions.net/api"
    private lateinit var mContext: Context


    private val PERMISSION_REQ_ID = 22
    private val viewModel: MainViewModel by viewModels()
    private val bluetoothAdapter: BluetoothAdapter? by lazy {
        BluetoothAdapter.getDefaultAdapter()
    }

    private fun getRequiredPermissions(): Array<String> {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            arrayOf(
                Manifest.permission.RECORD_AUDIO,
                Manifest.permission.READ_PHONE_STATE,
                Manifest.permission.BLUETOOTH_CONNECT,
                Manifest.permission.POST_NOTIFICATIONS
            )
        } else {
            arrayOf(
                Manifest.permission.RECORD_AUDIO,
                Manifest.permission.READ_PHONE_STATE
            )
        }
    }

    private fun startForegroundService() {
        Intent(applicationContext, RunningServices::class.java).also {
            it.action = RunningServices.Actions.START.name
            startService(it)
        }
    }

    private fun stopForegroundService() {
        Intent(applicationContext, RunningServices::class.java).also {
            it.action = RunningServices.Actions.STOP.name
            startService(it)
        }
    }

    private fun checkPermissions(): Boolean {
        for (permission in getRequiredPermissions()) {
            val permissionCheck = ContextCompat.checkSelfPermission(this, permission)
            if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                return false
            }
        }
        return true
    }

    private fun requestPermissions() {
        if (!checkPermissions()) {
            ActivityCompat.requestPermissions(
                this,
                getRequiredPermissions(),
                PERMISSION_REQ_ID
            )
        }
    }

    private fun isBluetoothConnected(): Boolean {
        return try {
            val bluetoothAdapter: BluetoothAdapter? = BluetoothAdapter.getDefaultAdapter()
            if (bluetoothAdapter != null && bluetoothAdapter.isEnabled) {
                val devices = bluetoothAdapter.getProfileConnectionState(BluetoothProfile.HEADSET)
                devices == BluetoothProfile.STATE_CONNECTED
            } else {
                false
            }
        } catch (e: SecurityException) {
            showMessage("Bluetooth permission not granted")
            false
        }
    }

    private fun setAudioToBluetooth() {
        try {
            val audioManager = getSystemService(Context.AUDIO_SERVICE) as AudioManager
            val connectedDevices = audioManager.getDevices(AudioManager.GET_DEVICES_OUTPUTS)
            val isBluetoothDeviceConnected = connectedDevices.any { it.type == AudioDeviceInfo.TYPE_BLUETOOTH_A2DP }

            if (isBluetoothDeviceConnected) {
                audioManager.mode = AudioManager.MODE_IN_COMMUNICATION
                audioManager.isBluetoothScoOn = true
                audioManager.isSpeakerphoneOn = false
            } else {
                showMessage("No Bluetooth device connected")
            }
        } catch (e: SecurityException) {
            showMessage("Bluetooth permission not granted")
        }
    }

    private fun setAudioToDefault() {
        try {
            val audioManager = getSystemService(Context.AUDIO_SERVICE) as AudioManager
            audioManager.mode = AudioManager.MODE_NORMAL
            audioManager.isBluetoothScoOn = false
            audioManager.isSpeakerphoneOn = true
        } catch (e: SecurityException) {
            showMessage("Bluetooth permission not granted")
        }
    }

    private fun showMessage(message: String) {
        runOnUiThread {
            Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT).show()
        }
    }

    private fun setupAgoraEngine(): Boolean {
        try {
            val config = RtcEngineConfig()
            config.mContext = mContext
            config.mAppId = appId
            config.mEventHandler = iRtcEngineEventHandler
            agoraEngine = RtcEngine.create(config)
        } catch (e: Exception) {
            showMessage(e.toString())
            return false
        }
        agoraEngine!!.setAudioProfile(
            Constants.AUDIO_PROFILE_MUSIC_HIGH_QUALITY,
            Constants.AUDIO_SCENARIO_MEETING
        )
        return true
    }
    private fun enableAINoiseSuppression() {
        // Noise reduction modes:
        // 0 -> Balance mode 
        // 1 -> Aggressive mode
        // 2 -> Aggressive mode with low latency
        val mode = 2 // Choose the appropriate mode based on your requirements

        // Set the mode for Audio AI Noise Suppression
        val result = agoraEngine?.setAINSMode(true, mode)
        if (result != Constants.ERR_OK) {
            showMessage("Failed to enable AI Noise Suppression. Error code: $result")
        } else {
            showMessage("AI Noise Suppression enabled successfully")
        }
    }


    private fun fetchToken(channelName: String, uid: Int): String? {
        val tokenRole = "broadcaster"
        val urlLString = "$serverUrl/rtc/$channelName/$tokenRole/uid/$uid"

        val client = OkHttpClient()
        val request: Request = Request.Builder()
            .url(urlLString)
            .header("Content-Type", "application/json; charset=UTF-8")
            .get()
            .build()

        val call = client.newCall(request)
        try {
            val response = call.execute()
            if (response.isSuccessful) {
                val responseBody = response.body()?.string()
                val jsonObject = JSONObject(responseBody)
                showMessage("Token fetched successfully")
                return jsonObject.getString("rtcToken")
            } else {
                showMessage("Token request failed")
            }
        } catch (e: IOException) {
            showMessage("IOException: $e")
        } catch (e: JSONException) {
            showMessage("Invalid token response")
        }
        return null
    }

    private val iRtcEngineEventHandler: IRtcEngineEventHandler = object : IRtcEngineEventHandler() {
        override fun onUserJoined(uid: Int, elapsed: Int) {
            showMessage("Remote user joined $uid")
            viewModel.addUser(uid.toString())
        }

        override fun onJoinChannelSuccess(channel: String, uid: Int, elapsed: Int) {
            viewModel.addUser(uid.toString())
            enableAINoiseSuppression()
            showMessage("Joined Channel $channel")
        }

        override fun onUserOffline(uid: Int, reason: Int) {
            showMessage("User $uid left the channel")
            viewModel.removeUser(uid.toString())
            viewModel.setLocalAudioMuted(false)
        }

        override fun onError(err: Int) {
            when (err) {
                ErrorCode.ERR_TOKEN_EXPIRED -> showMessage("Your token has expired")
                ErrorCode.ERR_INVALID_TOKEN -> showMessage("Your token is invalid")
                else -> showMessage("Error code: $err")
            }
        }
    }



    private fun joinChannel(channelName: String, uid: Int, onJoinSuccess: () -> Unit) {
        if (!checkPermissions()) {
            showMessage("Permissions were not granted")
            return
        }
        if (agoraEngine == null) setupAgoraEngine()

        val token = fetchToken(channelName, uid)
        val options = ChannelMediaOptions()
        options.channelProfile = Constants.CHANNEL_PROFILE_COMMUNICATION
        options.clientRoleType = Constants.CLIENT_ROLE_BROADCASTER

        agoraEngine?.joinChannel(token, channelName, uid, options)
        agoraEngine?.muteLocalAudioStream(false)
        agoraEngine?.muteAllRemoteAudioStreams(false)

        onJoinSuccess()
    }



    private fun toggleMute() {
        val newMuteState = !viewModel.isLocalAudioMuted.value!!
        viewModel.setLocalAudioMuted(newMuteState)
        agoraEngine?.muteLocalAudioStream(newMuteState)

        val message = if (newMuteState) "Local audio muted" else "Local audio un-muted"
        showMessage(message)
    }


    private fun leaveChannel(onLeaveSuccess: () -> Unit) {
        agoraEngine?.leaveChannel()
        viewModel.clearUsers()
        onLeaveSuccess()
    }

    private fun autoConnectBluetooth() {
        // Implement your logic to auto-connect Bluetooth here
        // For example, you can check paired devices and connect to a specific one
        // or start device discovery and connect to the desired device
        if (isBluetoothConnected()) {
            // Bluetooth is already connected, no action needed
            return
        }

        // If not connected, attempt to connect to Bluetooth device
        if (bluetoothAdapter?.isEnabled == true) {
            // Bluetooth is enabled, proceed with auto-connect logic
            // Example: Connect to a specific Bluetooth device
            val deviceToConnect = findBluetoothDeviceToConnect()
            if (deviceToConnect != null) {
                // Connect to the device
                // Example: deviceToConnect.connect()
            } else {
                // Device not found or no device to connect
                showMessage("Bluetooth device not found")
            }
        } else {
            // Bluetooth is not enabled
            showMessage("Bluetooth is not enabled")
        }
    }
    private fun findBluetoothDeviceToConnect(): BluetoothDevice? {
        // Implement your logic to find the Bluetooth device to connect
        // For example, scan for nearby devices and find the desired one
        // or check paired devices and select the desired one
        return null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mContext = this
        requestPermissions()

        if (bluetoothAdapter?.isEnabled == true) {
            autoConnectBluetooth()
        }

        setContent {
            var isUserJoined by remember { mutableStateOf(false) }
            var channelName by remember { mutableStateOf("") }
            val coroutineScope = rememberCoroutineScope()

            VoiceCallAppTheme {
                val viewModel: MainViewModel by viewModels() // Move viewModel initialization here

                if (!isUserJoined) {
                    VoiceCallRoom(
                        onJoinChannel = { channel, uid ->
                            joinChannel(channel, uid) {
                                isUserJoined = true
                                channelName = channel
                            }
                        },
                        onLeaveChannel = {
                            leaveChannel {
                                isUserJoined = false
                                channelName = ""
                            }
                        },
                        onAudioProfileChanged = { profile ->
                            agoraEngine?.setAudioProfile(profile, Constants.AUDIO_PROFILE_SPEECH_STANDARD)
                        },
                        isBluetoothConnected = isBluetoothConnected(),
                        setAudioToBluetooth = ::setAudioToBluetooth,
                        setAudioToDefault = ::setAudioToDefault,
                    )
                } else {
                    VoiceCallChannel(
                        onLeaveChannel = {
                            leaveChannel {
                                isUserJoined = false
                                channelName = ""
                            }
                        },
                        channelName = channelName,
                        remoteUsers = viewModel.remoteUsers,
                        coroutineScope = coroutineScope,
                        setAudioToDefault = ::setAudioToDefault,
                        isLocalAudioMuted= viewModel.isLocalAudioMuted,
                        toggleMute = ::toggleMute
                    )
                }
            }
            startForegroundService()
        }

        if (setupAgoraEngine()) {
            showMessage("Agora Engine successfully initialized")
        } else {
            showMessage("Failed to initialize Agora Engine")
        }
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PERMISSION_REQ_ID) {
            if (grantResults.isNotEmpty() && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                showMessage("Permissions were not granted")
            } else {
                showMessage("Permissions granted")
            }
        }
    }

    override fun onResume() {
        super.onResume()
        stopForegroundService()
    }

    override fun onPause() {
        super.onPause()
        startForegroundService()
    }


    override fun onDestroy() {
        super.onDestroy()
        stopForegroundService()
        agoraEngine?.leaveChannel()
        RtcEngine.destroy()
        agoraEngine = null
    }
}

