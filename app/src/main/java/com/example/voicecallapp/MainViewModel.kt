package com.example.voicecallapp

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MainViewModel : ViewModel() {
    private val _remoteUsers = MutableLiveData<List<String>>(emptyList())
    val remoteUsers: LiveData<List<String>> = _remoteUsers

    private val _isLocalAudioMuted = MutableLiveData<Boolean>(false)
    val isLocalAudioMuted: LiveData<Boolean> = _isLocalAudioMuted

    fun addUser(user: String) {
        val updatedList = _remoteUsers.value?.toMutableList() ?: mutableListOf()
        updatedList.add(user)
        _remoteUsers.postValue(updatedList)
    }

    fun removeUser(user: String) {
        val updatedList = _remoteUsers.value?.toMutableList() ?: mutableListOf()
        updatedList.remove(user)
        _remoteUsers.postValue(updatedList)
    }

    fun clearUsers() {
        _remoteUsers.postValue(emptyList())
    }

    fun setLocalAudioMuted(muted: Boolean) {
        _isLocalAudioMuted.postValue(muted)
    }
}




