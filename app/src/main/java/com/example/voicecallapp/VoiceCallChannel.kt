package com.example.voicecallapp

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Person
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.LiveData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


@Composable
fun VoiceCallChannel(
    onLeaveChannel: () -> Unit,
    channelName: String,
    remoteUsers: LiveData<List<String>>,
    coroutineScope: CoroutineScope,
    setAudioToDefault: () -> Unit,
    isLocalAudioMuted: LiveData<Boolean>,
    toggleMute: () -> Unit // Function to toggle mute
) {
    val users by remoteUsers.observeAsState(emptyList())
    val localAudioMuted by isLocalAudioMuted.observeAsState(false) // Observe the local audio mute state

    Box(
        modifier = Modifier.fillMaxSize()
    ) {
        Column(
            modifier = Modifier
                .align(Alignment.TopCenter)
                .padding(16.dp)
        ) {
            Text(
                text = channelName,
                fontSize = 24.sp,
                fontWeight = FontWeight.Bold
            )
        }

        Column(
            modifier = Modifier.align(Alignment.Center)
        ) {
            users.forEach { user ->
                Row(
                    modifier = Modifier
                        .padding(vertical = 8.dp)
                        .fillMaxWidth(),
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.Center
                ) {
                    Icon(
                        imageVector = Icons.Default.Person,
                        contentDescription = null,
                        modifier = Modifier
                            .padding(end = 8.dp)
                            .height(24.dp)
                            .aspectRatio(1f, true)
                    )
                    Text(
                        text = user,
                        fontSize = 18.sp
                    )
                }
            }
        }

        Row(
            modifier = Modifier
                .align(Alignment.BottomCenter)
                .padding(16.dp)
        ) {
            Button(
                onClick = {
                    coroutineScope.launch(Dispatchers.IO) {
                        toggleMute() // Toggle mute
                    }
                },
                colors = ButtonDefaults.buttonColors(
                    containerColor = Color(0xFF1976D2),
                    contentColor = Color.White
                ),
                modifier = Modifier.weight(1f)
            ) {
                Text(if (localAudioMuted) "Unmute" else "Mute") // Change button text dynamically
            }

            Button(
                onClick = {
                    coroutineScope.launch(Dispatchers.IO) {
                        onLeaveChannel()
                        setAudioToDefault()
                    }
                },
                colors = ButtonDefaults.buttonColors(
                    containerColor = Color(0xFFFF455B),
                    contentColor = Color.White
                ),
                modifier = Modifier.weight(1f)
            ) {
                Text("Leave")
            }
        }
    }
}
