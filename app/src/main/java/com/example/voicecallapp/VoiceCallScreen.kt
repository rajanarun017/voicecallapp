package com.example.voicecallapp

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import io.agora.rtc2.Constants
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

@Composable
fun VoiceCallRoom(
    onJoinChannel: (String, Int) -> Unit,
    onLeaveChannel: () -> Unit,
    onAudioProfileChanged: (Int) -> Unit,
    isBluetoothConnected: Boolean,
    setAudioToBluetooth: () -> Unit,
    setAudioToDefault: () -> Unit,
) {
    val coroutineScope = rememberCoroutineScope()
    var channelName by remember { mutableStateOf("") }
    var selectedProfile by remember { mutableStateOf(Constants.AUDIO_PROFILE_SPEECH_STANDARD) }
    var uid by remember { mutableStateOf("") }

    Column(
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier.fillMaxSize()
    ) {

        TextField(
            value = uid,
            onValueChange = { uid = it },
            label = { Text("Enter UID") },
            modifier = Modifier.padding(16.dp),
            colors= TextFieldDefaults.colors(
                focusedContainerColor = Color.White,
                unfocusedContainerColor = Color.White,
                focusedTextColor = Color.Black,
                unfocusedTextColor = Color.Black
            )
        )

        TextField(
            value = channelName,
            onValueChange = { channelName = it },
            label = { Text("Enter Channel Name") },
            modifier = Modifier.padding(16.dp),
            colors = TextFieldDefaults.colors(
                focusedContainerColor = Color.White,
                unfocusedContainerColor = Color.White,
                focusedTextColor = Color.Black,
                unfocusedTextColor = Color.Black
            )
        )


//        Column(
//            verticalArrangement = Arrangement.spacedBy(16.dp),
//            horizontalAlignment = Alignment.CenterHorizontally,
//            modifier = Modifier.padding(8.dp)
//        ) {
//            Row(
//                horizontalArrangement = Arrangement.spacedBy(8.dp),
//                verticalAlignment = Alignment.CenterVertically
//            ) {
//                RadioButton(
//                    selected = selectedProfile == Constants.AUDIO_PROFILE_SPEECH_STANDARD,
//                    onClick = { selectedProfile = Constants.AUDIO_PROFILE_SPEECH_STANDARD }
//                )
//                Text("Speech Standard")
//            }
//
//            Row(
//                horizontalArrangement = Arrangement.spacedBy(8.dp),
//                verticalAlignment = Alignment.CenterVertically
//            ) {
//                RadioButton(
//                    selected = selectedProfile == Constants.AUDIO_PROFILE_MUSIC_STANDARD,
//                    onClick = { selectedProfile = Constants.AUDIO_PROFILE_MUSIC_STANDARD }
//                )
//                Text("Music Standard")
//            }
//        }

        Spacer(modifier = Modifier.height(16.dp))

        Row(
            horizontalArrangement = Arrangement.Center,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Button(onClick = {
                coroutineScope.launch(Dispatchers.IO) {
                    val uidInt = uid.toIntOrNull()?:0
                    onJoinChannel(channelName, uidInt)
                    if (isBluetoothConnected) {
                        setAudioToBluetooth()
                    }
                }
            },
               colors= ButtonDefaults.buttonColors(

                   containerColor = Color(0xFF44C26B),
                   contentColor = Color.White
               )

                ) {
                Text("Join")
            }
        }

        // Call setAudioProfile whenever the selected profile changes
//        LaunchedEffect(selectedProfile) {
//            onAudioProfileChanged(selectedProfile)
//        }
    }
}
